// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeGameV01.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeGameV01, "SnakeGameV01" );
