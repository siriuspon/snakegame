// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameV01GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAMEV01_API ASnakeGameV01GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
